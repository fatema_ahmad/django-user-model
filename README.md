# Customizing User Model

## Substituting custom User model
* override the built-in User model where email address is used identification token instead of a username.
````
  AUTH_USER_MODEL = 'myapp.NewUser'
  ````
## Specifying a custom user model
* set up the custom user model that inherits from AbstractBaseUser
  - USERNAME_FIELD (A string describing the name of the field on the user model that is used as the unique identifier)
  - REQUIRED_FIELD (A list of the field names that will be prompted for when creating a user via the createsuperuser management) command.
  ````
    from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

    class NewUser(AbstractBaseUser, PermissionsMixin):
        email = models.EmailField(_('email address'), unique=True)
        user_name = models.CharField(max_length=150, unique=True)
        first_name = models.CharField(max_length=150, blank=True)
        start_date = models.DateTimeField(default=timezone.now)
        about = models.TextField(_(
            'about'), max_length=500, blank=True)
        is_staff = models.BooleanField(default=False)
        is_active = models.BooleanField(default=False)

        USERNAME_FIELD = 'email'
        REQUIRED_FIELDS = ['user_name', 'first_name']
  ````
## Writing a manager for a custom user model
*  define a custom manager that extends BaseUserManager providing two additional methods_
````
  from django.contrib.auth.models import BaseUserManager

  class CustomAccountManager(BaseUserManager):
      def create_superuser(self, email, user_name, first_name, password, **other_fields):
          other_fields.setdefault('is_staff', True)
          other_fields.setdefault('is_superuser', True)
          other_fields.setdefault('is_active', True)

          if other_fields.get('is_staff') is not True:
              raise ValueError(
                  'Superuser must be assigned to is_staff=True.')
          if other_fields.get('is_superuser') is not True:
              raise ValueError(
                  'Superuser must be assigned to is_superuser=True.')
          return self.create_user(email, user_name, first_name, password, **other_fields)

      def create_user(self, email, user_name, first_name, password, **other_fields):
          if not email:
              raise ValueError(_('You must provide an email address'))

          email = self.normalize_email(email)
          user = self.model(email=email, user_name=user_name,first_name=first_name, **other_fields)
          user.set_password(password)
          user.save()
          return user

    class NewUser(AbstractBaseUser, PermissionsMixin):
      objects = CustomAccountManager()
  ````
## Customizing the Django Admin interface
* define a UserAdminConfig class which is a subclass of django.contrib.auth.admin.UserAdmin, to add custom fields_
  - fieldsets (for fields to be used in editing users)
  - add_fieldsets (for fields to be used when creating a user)
  ````
    from .models import NewUser
    from django.contrib.auth.admin import UserAdmin

    class UserAdminConfig(UserAdmin):
      fieldsets = (
        (None, {'fields': ('email', 'user_name', 'first_name',)}),
            ('Permissions', {'fields': ('is_staff', 'is_active')}),
            ('Personal', {'fields': ('about',)}),
        )
        add_fieldsets = (
              (None, {
                  'classes': ('wide',),
                  'fields': ('email', 'user_name', 'first_name', 'password1', 'password2', 'is_active', 'is_staff')}
               ),
          )
    admin.site.register(NewUser,UserAdminConfig)
    ````
